const {
	Error400,
	Error401,
	Error403,
	Error404,
	Error405,
	Error409,
	Error500,
	Error501,
} = require('./error');

// -----------------------------------------------------------------------------

module.exports = {
	throwIf,
	throwErrorIf,
	throwError400If,
	throwError401If,
	throwError403If,
	throwError404If,
	throwError405If,
	throwError409If,
	throwError500If,
	throwError501If,
};

// -----------------------------------------------------------------------------

function throwIf(truthy, message) {
	throwErrorIf(truthy, Error, message);
}

function throwErrorIf(truthy, ErrorClass, message, userSafeMessage) {
	if (truthy) {
		throw new ErrorClass(message, userSafeMessage);
	}
}

// -----------------------------------------------------------------------------

function throwError400If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error400, message, userSafeMessage);
}

function throwError401If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error401, message, userSafeMessage);
}

function throwError403If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error403, message, userSafeMessage);
}

function throwError404If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error404, message, userSafeMessage);
}

function throwError405If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error405, message, userSafeMessage);
}

function throwError409If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error409, message, userSafeMessage);
}

// -----------------------------------------------------------------------------

function throwError500If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error500, message, userSafeMessage);
}
function throwError501If(truthy, message, userSafeMessage) {
	throwErrorIf(truthy, Error501, message, userSafeMessage);
}
