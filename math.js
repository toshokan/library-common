module.exports = {
	clamp,
	mod,
};

// -----------------------------------------------------------------------------

function clamp(number, minimum, maximum) {
	return Math.min(Math.max(number, minimum), maximum);
}

function mod(number, value) {
	return (number % value + value) % value;
}
