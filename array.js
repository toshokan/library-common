module.exports = {
	forEach,
	map,
};

// -----------------------------------------------------------------------------

function forEach(arr, iterator) {
	for (let index = 0; index < arr.length; index += 1) {
		iterator(arr[index], index);
	}
}

function map(arr, iterator) {
	const returnValues = [];

	for (let index = 0; index < arr.length; index += 1) {
		returnValues.push(iterator(arr[index], index));
	}

	return returnValues;
}
