module.exports = {
	orArr,
	orObj,
	orValue,
};

// -----------------------------------------------------------------------------

function orArr(arr) {
	return arr || [];
}

function orObj(value) {
	return orValue(value, {});
}

function orValue(value, defaultValue) {
	return value ? value : defaultValue;
}
